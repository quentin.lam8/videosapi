# VIDEOS APIs
Basic APIs for video management

Project is created with :
* springboot 2.7.1
* JAVA 11
* Maven 3.8.4
* JUNIT4
* In-memory DB H2
 

## Table of content
[[_TOC_]]
##  Add a video 
API to insert a video : 
![add video img](img/add_video.png)

##  Get a video 
API to get a video : 
![get video img](img/get_video.png)

##  Delete a video
API to delete a video : 
![delete video img](img/delete_video.png)

API to get deleted videos :
![get deleted video img](img/get_deleted_video.png)

##  Get Movies/Series
APi to get movies :
![get movie img](img/get_movie.png)

##  Get similar videos
APi to get similar videos :
![get similar video img](img/get_similar_video.png)


