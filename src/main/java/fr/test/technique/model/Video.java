package fr.test.technique.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "video")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SQLDelete(sql = "UPDATE video SET deleted = true WHERE id=?")
@FilterDef(name = "deletedVideoFilter", parameters = @ParamDef(name = "isDeleted", type = "boolean"))
@Filter(name = "deletedVideoFilter", condition = "deleted = :isDeleted")
@Setter
@Getter
@ToString
public class Video {

    @Id
    @GeneratedValue
    @Type(type="org.hibernate.type.UUIDCharType")
    private UUID id;

    @NotNull
    @Column (name = "title")
    private String title;

    @NotNull
    @ElementCollection
    @Column(name = "labels")
    private Set<String> labels;

    private boolean deleted = Boolean.FALSE;
}
