package fr.test.technique.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.SQLDelete;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "movie")
@SQLDelete(sql = "UPDATE MOVIE SET deleted = true WHERE id=?")
@FilterDef(name = "deletedVideoFilter", parameters = @ParamDef(name = "isDeleted", type = "boolean"))
@Filter(name = "deletedVideoFilter", condition = "deleted = :isDeleted")
@Setter
@Getter
@ToString
public class Movie extends Video{

    @NotNull(message = "Director name is missing")
    @Column(name = "director" )
    private String director;

    @NotNull(message = "Release Date is missing")
    @Column(name = "release_date")
    @JsonProperty("release_date")
    private Date releaseDate;
}
