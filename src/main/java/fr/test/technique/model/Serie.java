package fr.test.technique.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.SQLDelete;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "serie")
@SQLDelete(sql = "UPDATE serie SET deleted = true WHERE id=?")
@FilterDef(name = "deletedVideoFilter", parameters = @ParamDef(name = "isDeleted", type = "boolean"))
@Filter(name = "deletedVideoFilter", condition = "deleted = :isDeleted")
@Setter
@Getter
@ToString
public class Serie extends Video{

    @NotNull
    @Column(name = "number_of_episodes")
    @JsonProperty("number_of_episodes")
    private Integer numberOfEpisodes;
}
