package fr.test.technique.controller;

import fr.test.technique.model.Video;
import fr.test.technique.service.impl.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/video")
public class VideoController {

    @Autowired
    VideoService videoService;

    /**
     * Get all videos
     * @return
     */
    @GetMapping
    public ResponseEntity<List<Video> > getVideos(){
        List<Video> videos = videoService.getAllVideos();

        if(!videos.isEmpty()){
            return new ResponseEntity<>(videos, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Get video by id
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public ResponseEntity<Video> getVideoById(@PathVariable UUID id){
        Optional<Video> video = videoService.getById(id);

        if(video.isPresent()){
            return new ResponseEntity<>(video.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Get video by title
     * @param title
     * @return
     */
    @GetMapping("/title/{title}")
    public ResponseEntity<List<Video>> getVideoByName(@PathVariable String title){
        List<Video> videos = videoService.getByTitle(title);

        if (!videos.isEmpty()){
            return new ResponseEntity<>(videos, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Get similar videos having same label
     * @param labels
     * @param nbLabelMin number of label that has to be in common
     * @return
     */
    @GetMapping("/label/{nbLabelMin}")
    public ResponseEntity<Set<Video>> getVideoByLabel(@RequestBody Set<String> labels, @PathVariable int nbLabelMin){
        Set<Video> videos = videoService.getSimilar(labels, nbLabelMin);

        if (!videos.isEmpty()){
            return new ResponseEntity<>(videos, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Save a new video
     * @param video
     * @return
     */
    @PostMapping()
    public ResponseEntity<Video> save(@Valid @RequestBody Video video){

        try {
            return new ResponseEntity<>(videoService.save(video), HttpStatus.CREATED);
        } catch (Exception e){
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Delete a video
     * @param id
     */
    @DeleteMapping("/{id}")
    public void deleteVideoById(@PathVariable UUID id){
        videoService.deleteById(id);
    }

    /**
     *
     * Get list of deleted videos
     * @return
     */
    @GetMapping("/deletedList")
    public ResponseEntity<List<Video>> getDeletedList(){
        List<Video> videos = videoService.getDeleted();
        if (!videos.isEmpty()){
            return new ResponseEntity<>(videos, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Exception when body not valid
     * @param ex
     * @return
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
