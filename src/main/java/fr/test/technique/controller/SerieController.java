package fr.test.technique.controller;

import fr.test.technique.model.Serie;
import fr.test.technique.service.impl.SerieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/serie")
public class SerieController {
    
    @Autowired
    SerieService serieService;

    /**
     * Get all series
     * @return
     */
    @GetMapping
    public ResponseEntity<List<Serie>> getSeries(){
        List<Serie> series = serieService.getAllVideos();

        if(!series.isEmpty()){
            return new ResponseEntity<>(series, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Save new serie
     * @param serie
     * @return
     */
    @PostMapping()
    public ResponseEntity<Serie> save(@Valid @RequestBody Serie serie){
        try {
            return new ResponseEntity<>(serieService.save(serie), HttpStatus.CREATED);
        } catch (Exception e){
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Exception when body not valid
     * @param ex
     * @return
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
