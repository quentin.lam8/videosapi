package fr.test.technique.repository;

import fr.test.technique.model.Video;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Generic repository
 * @param <T>
 */
@Repository
public interface VideoBaseRepo<T extends Video> extends JpaRepository<T, UUID> {

    /**
     * Find video by title
     * @param title
     * @return list of videos
     */
    List<T> findVideoByTitle(String title);

    /**
     * Find video containing title
     * @param title
     * @return list of videos
     */
    List<T> findVideoByTitleContaining(String title);

    /**
     * Get videos having labels
     * @param labels
     * @return set of videos
     */
    Set<T> findVideoByLabelsIn(Set<String> labels);

}
