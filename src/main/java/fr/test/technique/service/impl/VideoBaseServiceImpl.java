package fr.test.technique.service.impl;

import fr.test.technique.model.Video;
import fr.test.technique.repository.VideoBaseRepo;
import fr.test.technique.service.VideoBaseService;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Generic service video
 * @param <T>
 */
public class VideoBaseServiceImpl<T extends Video> implements VideoBaseService<T> {

    @Autowired
    private VideoBaseRepo<T> dao;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<T> getAllVideos(){
        return getAllDeletedFilter(Boolean.FALSE);
    }

    @Override
    public Optional<T> getById(UUID id){
        return dao.findById(id);
    }

    @Override
    public List<T> getByTitle(String title){
        if(title.length() >= 3){
            return dao.findVideoByTitleContaining(title);
        } else {
            return dao.findVideoByTitle(title);
        }
    }
    @Override
    public T save(T video){
        return dao.save(video);
    }

    @Override
    public void deleteById(UUID id){
        dao.deleteById(id);
    }

    @Override
    public List<T> getDeleted(){
        return getAllDeletedFilter(Boolean.TRUE);
    }

    @Override
    public Set<T> getSimilar(Set<String> labels, int nbLabelMin){
        Set<T> unfilteredList = dao.findVideoByLabelsIn(labels);

        return unfilteredList.stream()
                .distinct()
                .filter(l -> {
                    Set<String> tmpLabels =  l.getLabels();
                    tmpLabels.retainAll(labels);
                    return tmpLabels.size() >= nbLabelMin;
                })
        .collect(Collectors.toSet());
    }

    private List<T> getAllDeletedFilter(boolean isDeleted){
        Session session = entityManager.unwrap(Session.class);
        Filter filter = session.enableFilter("deletedVideoFilter");
        filter.setParameter("isDeleted", isDeleted);
        List<T> videos = dao.findAll();
        session.disableFilter("deletedVideoFilter");
        return videos;
    }

}
