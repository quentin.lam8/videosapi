package fr.test.technique.service.impl;

import fr.test.technique.model.Serie;
import org.springframework.stereotype.Service;

@Service
public class SerieService extends VideoBaseServiceImpl<Serie> {
}
