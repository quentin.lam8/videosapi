package fr.test.technique.service.impl;

import fr.test.technique.model.Movie;
import org.springframework.stereotype.Service;

@Service
public class MovieService extends VideoBaseServiceImpl<Movie> {

}
