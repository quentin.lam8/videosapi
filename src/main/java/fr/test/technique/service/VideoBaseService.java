package fr.test.technique.service;

import fr.test.technique.model.Video;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * Generic service video
 * @param <T>
 */
public interface VideoBaseService<T extends Video>{
    /**
     * Get all videos
     * @return a list videos
     */
    List<T> getAllVideos();

    /**
     * Get video by id
     * @param id
     * @return a video
     */
    Optional<T> getById(UUID id);

    /**
     * Get video by title
     * @param title
     * @return a list of video
     */
    List<T> getByTitle(String title);

    /**
     * Persist a video
     * @param video
     * @return video saved
     */
    T save(T video);

    /**
     * Delete a video using id
     * @param id
     */
    void deleteById(UUID id);

    /**
     * Get list of deleted videos
     * @return
     */
    List<T> getDeleted();

    /**
     * Get list of similar videos using labels
     * @param labels
     * @param nbLabelMin
     * @return
     */
    Set<T> getSimilar(Set<String> labels, int nbLabelMin);

}
