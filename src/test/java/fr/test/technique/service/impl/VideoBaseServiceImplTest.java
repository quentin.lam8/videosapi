package fr.test.technique.service.impl;

import fr.test.technique.model.Video;
import fr.test.technique.repository.VideoBaseRepo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

@RunWith(MockitoJUnitRunner.class)
public class VideoBaseServiceImplTest {

    @Mock
    VideoBaseRepo dao;

    @InjectMocks
    VideoService videoService;

    @Test
    public void getSimilarTest(){
        Set<String> labels = new HashSet<>(Set.of("adventure", "horror"));
        int nbLabelMin = 2;

        //videos returned by dao
        Set<Video> videos = new HashSet<>();
        Video video1 = new Video();
        video1.setTitle("video 1");
        video1.setLabels(new HashSet<>(Set.of("adventure", "horror", "fantasy")));
        videos.add(video1);

        Video video2 = new Video();
        video2.setTitle("video 2");
        video2.setLabels(new HashSet<>(Set.of("adventure", "comedy", "fantasy")));
        videos.add(video2);

        Video video3 = new Video();
        video3.setTitle("video 3");
        video3.setLabels(new HashSet<>(Set.of("adventure", "comedy", "horror")));
        videos.add(video3);

        Mockito.when(dao.findVideoByLabelsIn(labels))
                .thenReturn(videos);

        Assert.assertEquals("Didn't find the right number of videos",2, videoService.getSimilar(labels, nbLabelMin).size());
    }
}
